<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/functions.php';





class libpayment_Controller extends bab_Controller
{

    protected function getControllerTg()
    {
        return 'addon/libpayment/main';
    }
    
    
    /**
     * Get object name to use in URL from the controller classname
     * @param string $classname        Does not include the namespace
     * @return string
     */
    protected function getObjectName($classname)
    {
        return strtolower(substr($classname, strlen('libpayment_Ctrl')));
    }
    
    
    /**
     * 
     * @return libpayment_CtrlLog
     */
    public function Log($proxy = true)
    {
        require_once dirname(__FILE__) . '/ctrl/log.ctrl.php';
        return bab_Controller::ControllerProxy('libpayment_CtrlLog', $proxy);
    }

}