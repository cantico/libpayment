<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/


require_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';
require_once dirname(__FILE__) . '/payment.event.php';
require_once dirname(__FILE__) . '/functions.php';




/**
 * Command line interface to authorize
 * One payment manually in case of failure 
 * 
 * su -s /bin/bash -c 'php -f index.php "tg=addon/LibPayment/authorize&fn=Paybox&token=57fa017ba34879.25734208&amount=107.17&authorization=502761&transaction=658497283"' apache
 * 
 * ou sudo -u www-data ...
 */

if (php_sapi_name() !== 'cli') {
    throw new Exception('This is a command line tool');
}

$token = bab_rp('token', null);
$paymentLog = libpayment_getPaymentLog($token);

bab_functionality::includeOriginal('Payment/'.bab_rp('fn'));

$paymentEvent = new libpayment_EventPaymentSuccess();

$paymentEvent->setPayment($paymentLog->getPayment());
$paymentEvent->setResponseAmount(bab_rp('amount'));
$paymentEvent->setResponseAuthorization(bab_rp('authorization'));
$paymentEvent->setResponseTransaction(bab_rp('transaction'));

bab_fireEvent($paymentEvent);