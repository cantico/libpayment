<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/


bab_Widgets()->includePhpClass('Widget_Form');


class libpayment_SettingsEditor extends Widget_Form
{
    public function __construct($id = null, Widget_Layout $layout = null)
    {
        parent::__construct($id, $layout);
        
        $this->setName('settings');
        $this->setHiddenValue('tg', bab_rp('tg'));
        
        $this->addFields();
        $this->addButtons();
        $this->setValues($this->getSettings());
    }
    
    
    protected function getSettings()
    {
        $registry = libpayment_Registry();
        $values = array();
        
        while($key = $registry->fetchChildKey()) {
            $values[$key] = $registry->getValue($key);
        }
        
        $values['limitAmountDuration'] = $registry->getValue('limitAmountDuration');
        $values['limitAmountTotal'] = $registry->getValue('limitAmountTotal');
        $values['limitTransactionDuration'] = $registry->getValue('limitTransactionDuration');
        $values['limitTransactionTotal'] = $registry->getValue('limitTransactionTotal');
        $values['email'] = $registry->getValue('email');
        
        return array('settings' => $values);
    }
    
    
    protected function addFields()
    {
        $this->addItem($this->limitAmount());
        $this->addItem($this->limitTransaction());
        $this->addItem($this->email());
    }
    
    
    protected function addButtons()
    {
        $W = bab_Widgets();
        $ctrl = libpayment_Controller()->Log();
        $this->addItem($W->SubmitButton()
            ->setAction($ctrl->saveSettings())
            ->setLabel(libpayment_translate('Save'))
        );
    }
        
    
    protected function limitAmount()
    {
        $W = bab_Widgets();
        
        $value = $W->LabelledWidget(
            libpayment_translate('Alert if total amount greater than'),
            $W->NumberEdit(), 
            'limitAmountTotal'
        );
        
        $duration = $W->LabelledWidget(
            libpayment_translate('On a period duration of'),
            $W->NumberEdit(),
            'limitAmountDuration',
            null,
            libpayment_translate('Hours')
        );
        
        
        return $W->FlowItems($value, $duration)
            ->setVerticalAlign('top')
            ->setSpacing(4, 'em');
    }
    
    
    
    protected function limitTransaction()
    {
        $W = bab_Widgets();
    
        $value = $W->LabelledWidget(
            libpayment_translate('Alert if the number of transaction is greater than'),
            $W->NumberEdit(),
            'limitTransactionTotal'
        );

        $duration = $W->LabelledWidget(
            libpayment_translate('On a period duration of'),
            $W->NumberEdit(),
            'limitTransactionDuration',
            null,
            libpayment_translate('Hours')
        );


        return $W->FlowItems($value, $duration)
            ->setVerticalAlign('top')
            ->setSpacing(4, 'em');
    }
    
    
    protected function email()
    {
        $W = bab_Widgets();
        
        return $W->LabelledWidget(
            libpayment_translate('Email'),
            $W->LineEdit()->addClass('widget-100pc'),
            __FUNCTION__,
            libpayment_translate('Comma separated emails')
        );
    }
}