<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/


bab_Widgets()->includePhpClass('Widget_TableModelView');

// all payment types for deserialization

require_once dirname(__FILE__).'/../payment/payment.class.php';
require_once dirname(__FILE__).'/../payment/bankpayment.class.php';
require_once dirname(__FILE__).'/../payment/cardpayment.class.php';
require_once dirname(__FILE__).'/../payment/recurringpayment.class.php';
require_once dirname(__FILE__).'/../payment/recurringcontractdetail.class.php';


class libpayment_LogTableView extends widget_TableModelView
{
    public function addDefaultColumns(ORM_RecordSet $set)
    {
        bab_functionality::includeOriginal('Icons');
        $this->addClass(Func_Icons::ICON_LEFT_24);
    
        /*@var $set payment_logSet */
    
        $this->addColumn(widget_TableModelViewColumn($set->time, libpayment_translate('Date')));
        $this->addColumn(widget_TableModelViewColumn($set->token, libpayment_translate('Token'))->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn($set->reference, libpayment_translate('Gateway reference')));
        $this->addColumn(widget_TableModelViewColumn($set->user, libpayment_translate('User'))->setVisible(false)->setSearchable(false));
    
        $this->addColumn(widget_TableModelViewColumn($set->payment, libpayment_translate('Request')));
        $this->addColumn(widget_TableModelViewColumn($set->response, libpayment_translate('Response')));
    }
    
    
    /**
     * @param ORM_Record	$record
     * @param string		$fieldPath
     * @return Widget_Item
     */
    protected function computeCellTextContent(ORM_Record $record, $fieldPath)
    {
  
        if ('payment' === $fieldPath)
        {
            if (!$record->payment) {
                return '';
            }
            
            $payment = unserialize($record->payment);
            /*@var $payment libpayment_Payment */
            return $payment->toString();
        }
    
        
        if ('user' === $fieldPath) {
            return bab_getUserName($record->user);
        }
    
    
        return parent::computeCellTextContent($record, $fieldPath);
    }
    
    
    
    /**
     * @param ORM_Record	$record
     * @param string		$fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();
        
        $cellContent = $W->Html(bab_toHtml($this->computeCellTextContent($record, $fieldPath), BAB_HTML_ALL));
        return $cellContent;
    }
    
    
    
}